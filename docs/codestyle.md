## Nodejs Coding Style

This is a guide for writing consistent and aesthetically pleasing node.js code. It is inspired by code already written on internaly at meetic for chat-server and the coding style recommendation within the node.js community.
There is a .jshintrc which enforces these rules as closely as possible. You can either adjust it to fit your needs.

### side note:
The long-term value of software to an organization is in direct proportion to the quality of the codebase. Over its lifetime, a program will be handled by many pairs of hands and eyes. If a program is able to clearly communicate its structure and characteristics, it is less likely that it will break when modified in the never-too-distant future.
Code conventions can help in reducing the brittleness of programs. 

### File naming
Lower camel case for multiple words names

###  Indentation
The unit of indentation is four spaces. Use of tabs should be avoided because there still is not a standard for the placement of tabstops.

### Line Length
Lines longer than 80 characters shuld be avoided. if a statement doesn't fit on a single line, it may be necessary to break it. 
Place the break after an operator, ideally after a comma. A break after an operator decreases the likelihood that a copy-paste error will be masked by semicolon insertion. 

### Comments
Comments should be written in a concise and clear language.
Generally use inline comments. //
For classes and function formal documentation use block comments /** **/


### Whitespace
 + A keyword followed by ( (left parenthesis) should be separated by a space.
 + A blank space should not be used between a function value and its ( (left parenthesis). 
 + All binary operators except . (period) and ( (left parenthesis) and [ (left bracket) should be separated from their operands by a space.
 + No space should separate a unary operator and its operand except when the operator is a word such as typeof.
 + Each ; (semicolon) in a for statement should be followed with a space.
 + Whitespace should follow every , (comma).


### Variables
All variables should be declared before used. makes the program easier to read and makes it easier to detect undeclared variables that may become implied globals. 
Use of global variables should be minimized. The var statement should be the first statement in the function body. 
It is preferred that each variable be given its own line and comment and listed in alphabetical order if possible.

    var currentEntry, // currently selected table entry
        level,        // indentation level
        size;         // size of table


### Functions
All functions should be declared before they are used. Inner functions should follow the var statement. This helps make it clear what variables are included in its scope
There should be no space between the name of a function and the ( (left parenthesis) of its parameter list. There should be one space between the ) (right parenthesis) and the { (left curly brace) that begins the statement body
If a function literal is anonymous, there should be one space between the word function and the ( (left parenthesis).



### Naming
Names should be formed from the 26 upper and lower case letters (A .. Z, a .. z), the 10 digits (0 .. 9), and _ (underbar). Avoid use of international characters because they may not read well or be understood everywhere. Do not use $ (dollar sign) or \ (backslash) in names. 
Most variables and functions should start with a lower case letter. 
Constructor functions that must be used with the new prefix should start with a capital letter.

### Statements
Simple statement: Each line should contain at most one statement. Put a ; (semicolon) at the end of every simple statement.

Compound statement: statements that contain lists of statements enclosed in { } (curly braces). 

+ The enclosed statements should be indented 2 spaces.
+ The { (left curly brace) should be at the end of the line that begins the compound statement.
+ The } (right curly brace) should begin a line and be indented to align with the beginning of the line containing the matching { (left curly brace).
+ Braces should be used around all statements, even single statements, when they are part of a control structure, such as an if or for statement. This makes it easier to add statements without accidentally introducing bugs.

Note: that an assignment statement that is assigning a function literal or object literal is still an assignment statement and must end with a semicolon. 

### Quotes
Single quotes are the norm unless it is necessary to use double quotes, like in JSON.

Right:

    var foo = 'bar';

Wrong:

    var foo = "bar";

### Cases
Variables, properties and function names should use lowerCamelCase. They should also be descriptive. Single character variables and uncommon abbreviations should generally be avoided.

Right:

    var adminUser = db.query('SELECT * FROM users ...');

Wrong:

    var admin_user = db.query('SELECT * FROM users ...');
    
    Class names should be capitalized using UpperCamelCase.

Right:

    function BankAccount() {
    }

Wrong:

    function bank_Account() {
    }

### === Operator
Right:

    var a = 0;
    if (a !== '') {
      console.log('winning');
    }

Wrong:

    var a = 0;
    if (a == '') {
      console.log('losing');
    }


### Conditions
Any non-trivial conditions should be assigned to a descriptively named variable or function:

Right:

    var isValidPassword = password.length >= 4 && /^(?=.*\d).{4,}$/.test(password);
    
    if (isValidPassword) {
      console.log('winning');
    }

Wrong:

    if (password.length >= 4 && /^(?=.*\d).{4,}$/.test(password)) {
      console.log('losing');
    }

### Closures
Right:

    req.on('end', function onEnd() {
      console.log('winning');
    });

Wrong:

    req.on('end', function() {
      console.log('losing');
    });


Right:

    setTimeout(function() {
      client.connect(afterConnect);
    }, 1000);
    
    function afterConnect() {
      console.log('winning');
    }

Wrong:

    setTimeout(function() {
      client.connect(function() {
        console.log('losing');
      });
    }, 1000);
