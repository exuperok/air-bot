'use strict';
//requiring node_modules
var commandArgs = require('minimist')(process.argv.slice(2));
var JobStarter = require('./component/job-starter.js');
var ConfigLoader = require('./component/config-builder.js');
var DiffBuilder = require('./component/diff-builder.js');
var GitlabClient = require('./component/gitlab-client.js');
var JenkinsClient = require('./component/jenkins-client.js');
// var Logger = require('./component/logger.js');

//instantiate the the process launcher
var jobStarter = new JobStarter(commandArgs);
var autojobName = jobStarter.startProcess();

//instantiate the configuration manager
if (typeof autojobName === 'string') {
  var oConfigLoader = new ConfigLoader(autojobName);
} else {
  console.log('Your tried to start a process with an invalid autojob name. Process is ending now.');
  process.exit(1);
}

//load and parse the configuration
try {
  var loadedConfig = oConfigLoader.loadConfig();
  var oConfiguration = oConfigLoader.parseConfig(loadedConfig);
} catch (e) {
  console.log(e.message);
}

//instantiate a gitlab client
var gitlabClient = new GitlabClient(oConfiguration.gitlab_conf);
gitlabClient.buildConnection();

//gitlab operations starts here
gitlabClient.retrieveProjectBranches().then(function() {
  if (gitlabClient.projectBranchesList instanceof Array) {
    //diff between current and previous brannches
    var diffBuilder = new DiffBuilder(gitlabClient.projectBranchesList, autojobName);
    diffBuilder.loadPrevList();
    diffBuilder.parsePrevList();
    diffBuilder.buildBranchDifff();

    //jenkins operation starts here
    var jenkinsClient = new JenkinsClient(oConfiguration.jenkins_conf,
    gitlabClient.projectBranchesList,autojobName,oConfiguration.system_conf);
    jenkinsClient.buildConnection();

    if(diffBuilder.branchDiff.deletion_list.length) {
      jenkinsClient.deleteProjectJob(diffBuilder.branchDiff.deletion_list, 0);
    }

    if(diffBuilder.branchDiff.creation_list.length) {
      jenkinsClient.createProjectJob(diffBuilder.branchDiff.creation_list, 0);
    }
  }
}, function(err) {
  console.error(err.message);
});
