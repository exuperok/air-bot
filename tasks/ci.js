'use strict';

module.exports = function(grunt) {
  grunt.registerTask('ci', 'Run the continuous integration tests & static analysis', function() {
    grunt.task.run(['clean:ci', 'check:ci', 'test:ci']);

  });
};
