'use strict';

module.exports = function(grunt) {
  grunt.registerTask('static', 'Validate files and coding syle', function() {
    var type = (this.args[0] || 'dev');
    grunt.task.run([
      'indus_complexity:' + type
    ]);
  });
};
