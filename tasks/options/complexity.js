'use strict';

/**
 * Created by j.calabrese on 11/13/14.
 */
module.exports = {
  dev: {
    options: {
      cyclomatic: {
        controller: 2,
        service: 2,
        other: 2
      },
      pmdXML: 'reports/violation/complexity.xml'
    },
    files: {
      default_options: [
        '**/*.js', 
        '!node_modules/**/*.js', 
        '!reports/**/*.js', 
        '!tasks/**/*.js',
        '!tests/**/*.js',
        '!Gruntfile.js'
      ]
    }
  },
  ci: {
    options: {
      cyclomatic: {
        controller: 2,
        service: 2,
        other: 2
      },
      pmdXML: 'reports/violation/complexity.xml'
    },
    files: {
      default_options: [
        '**/*.js', 
        '!node_modules/**/*.js', 
        '!reports/**/*.js', 
        '!tasks/**/*.js',
        '!tests/**/*.js',
        '!Gruntfile.js'
      ]
    }
  }
};