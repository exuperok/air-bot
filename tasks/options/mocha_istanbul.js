'use strict';

var fs         = require('fs');
var path       = require('path');
var reportPath = './reports/test';

process.env.JUNIT_REPORT_PATH = path.join(reportPath, '/report.xml');

// Create the report directory
var exists = fs.existsSync(reportPath);
if (!exists) {
  var dirs     = reportPath.split(path.sep);
  var fullPath = '';

  dirs.forEach(function(dir) {
    fullPath = path.join(fullPath, dir);
    try {
      fs.mkdirSync(fullPath);
    } catch (e) {}
  });
}

module.exports = {
  dev: {
    src: 'tests',
    options: {
      reporter: 'spec',
      excludes: ['Gruntfile.js', 'tasks/**'],
      coverageFolder: './reports/coverage',
      reportFormats: ['html']
    }
  },
  ci: {
    src: 'tests/',
    options: {
      reporter: 'mocha-jenkins-reporter',
      excludes: ['Gruntfile.js', 'tasks/**'],
      coverageFolder: './reports/coverage',
      reportFormats: ['clover','html']
    }
  }
};
