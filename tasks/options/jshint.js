'use strict';

module.exports = {
  dev: {
    options: {
      jshintrc: true
    },
    src: ['**/*.js']
  },
  ci: {
    options: {
      jshintrc: true,
      force: true,
      reporter: 'checkstyle',
      reporterOutput: './reports/jshint/jshint-result.xml'
    },
    src: ['**/*.js']
  }
};
