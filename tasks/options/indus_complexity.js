'use strict';

/**
 * Created by j.calabrese on 11/13/14.
 */
module.exports = {
  dev: {
    options: {
      cyclomatic: {
        builder: {
          threshold: 2,
          severity: 1
        },
        client: {
          threshold: 4,
          severity: 2
        },
        other:{
          threshold: 2,
          severity: 1
        }
      },
      severityLevels : [
        'notice',
        'warning',
        'error'
      ],
      pmdXML: 'reports/violation/complexity.xml'
    },
    files: {
      default_options: [
        '**/*.js',
        '!node_modules/**/*.js',
        '!reports/**/*.js',
        '!tasks/**/*.js',
        '!tests/**/*.js',
        '!Gruntfile.js'
      ]
    }
  },
  ci: {
    options: {
      cyclomatic: {
        builder: {
          threshold: 2,
          severity: 1
        },
        client: {
          threshold: 4,
          severity: 2
        },
        other:{
          threshold: 2,
          severity: 1
        }
      },
      severityLevels : [
        'notice',
        'warning',
        'error'
      ],
      pmdXML: 'reports/violation/complexity.xml'
    },
    files: {
      default_options: [
        '**/*.js',
        '!node_modules/**/*.js',
        '!reports/**/*.js',
        '!tasks/**/*.js',
        '!tests/**/*.js',
        '!Gruntfile.js'
      ]
    }
  }
};