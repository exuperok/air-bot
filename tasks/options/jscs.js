'use strict';

module.exports = {
  dev: {
    src: '**/*.js',
    options: {
      config: '.jscsrc'
    }
  },
  ci: {
    src: '**/*.js',
    options: {
      config: '.jscsrc',
      force: true,
      reporter: 'checkstyle',
      reporterOutput: './reports/jscs/jscs-result.xml'
    }
  }
};
