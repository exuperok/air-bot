'use strict';

module.exports = {
  dev: {
    src: [
      'reports/complexity/**',
      'reports/violation/**',
      'reports/coverage/**',
      'reports/jscs/**',
      'reports/jshint/**',
      'reports/test/**', '!reports/test'
    ]
  },
  ci: {
    src: [
      'reports/complexity/**',
      'reports/violation/**',
      'reports/coverage/**',
      'reports/jscs/**',
      'reports/jshint/**',
      'reports/test/**', '!reports/test'
    ]
  }
};
