'use strict';

module.exports = function(grunt) {
  grunt.registerTask('test', 'Run tests', function() {
    grunt.task.run('mocha_istanbul:' + (this.args[0] || 'dev'));
  });
};
