var JobStarter = require("../component/job-starter.js");
var assert = require("chai").assert;
var expect = require("chai").expect;
var sinon = require("sinon");

describe("startProcess", function () {
  var stringArgs = {};
  var nonStringArgs = {};

  before(function(){
    stringArgs = { _: [], c: 'sandbox' };
    nonStringArgs = { _: [], c: 2 };
  });

  it("should return a string if c argument is a string", function () {
    var jobStarter =  new JobStarter(stringArgs);
    var projectName = jobStarter.startProcess();
    expect(projectName).to.be.a('string');
  });

  it("should not throw error if c argument is a string", function () {
    var jobStarter =  new JobStarter(stringArgs);
    expect(function () {
      jobStarter.startProcess();
    }).to.not.throw(Error, /Your project name is not valid/);
  });

  it("should return project name of value sandbox if c argument is sandbox", function () {
    var jobStarter =  new JobStarter(stringArgs);
    var projectName = jobStarter.startProcess();
    expect(projectName).to.equal('sandbox');
  });

  it("should throw an error if the c argument is not a string", function () {
    var jobStarter =  new JobStarter(nonStringArgs);
    expect(Error).to.exist;
    expect(function () {
      jobStarter.startProcess();
    }).to.throw(Error, /Your project name is not valid/);
  });
});
