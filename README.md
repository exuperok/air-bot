## Jengit-Autojob
[![Build Status](https://travis-ci.org/exuperok/jengit-autojob.svg?branch=test-bdd-style-chai-eokouya)](https://travis-ci.org/exuperok/jengit-autojob)
### Description

Jengit-Autojob is an autoomation tools which allows the synchronisation of jobs and branches between a Gitlab Project and the corresponding Jenkins project. It is inspired by Jenkins Autojobs.

Jengit-Autojob follows these simples steps
 - Read json configuration file.
 - List branches from git repository.
 - Create jos if new branches have been created.
 - Delete jobs if old branches have been deleted

The Json configuration file provides the following info:
 - How to access Jenkins and the SCM repository.
 - Which template job to use for which branch.
 - Which branches to process and which to ignore. 
 - How new jobs should be named.
 
### coding style
(docs/codingstyle)

### Git Workflow
- 1 protected branche: master and develop
- Create branches for every feature or task from master
- Once finished and if the build is green, merge into master (through pull/merge request)
- For every release tag after merging to master

### Commit message guidelines
these guidelines have been strongly inspired by [Angular Guidelines](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md)_


### Testing & Quality
Mocha BDD: npm run test
Complexity: npm run static
Style: npm run style

### Deployment 
TBA

### Launching
cd /path/to/autojob/root && node autojob -c project-name

### Trouble Shooting
TBA

