'use strict';
var fsReader = require('fs');
var CONFIG_SUFFIX = '.config.json';
var CONFIG_FOLDER = './config/';

var ConfigBuilder = function ConfigBuilder(fileName) {
  if ((!fileName) || typeof fileName !== 'string') {
    console.log('Your project name is not valid');
    process.exit(1);
  }
  this.filename = fileName;
  this.projectConfig = {};
};

ConfigBuilder.prototype = {

  /**
   * Load the configuration file corresponding to the autojobName
   * @returns {*}
   */
  loadConfig: function loadConfig() {
    try {
      var loadedConfig = fsReader.readFileSync(CONFIG_FOLDER + this.filename + CONFIG_SUFFIX, 'utf8');
      if (loadedConfig !== undefined || typeof loadedConfig === 'string') {
        console.log('Your configuration file has been loaded');
      }
      return loadedConfig;
    } catch (e) {
      console.log('Something wrong occured while attempting to load your configuration.');
      console.log(e.message);
      process.exit(3);
    }
  },

  /**
   * parse the loaded configuration file to return an object literal
   */
  parseConfig: function parseConfig(loadedConfig) {
    if (typeof loadedConfig === 'string') {
      try {
        var projectConfig = JSON.parse(loadedConfig);
        if (typeof projectConfig === 'object') {
          console.log('Your configuration data have been parsed into a an object litteral');
          return projectConfig;
        }
      } catch (e) {
        console.log('Autojob failed to parse your configurtation file. Not a valid json document.');
        console.log(e.message);
        process.exit(2);
      }
    }
  },

  //@todo: check the entire configuration here
  checkConfigData: function checkConfigData(configData) {
    if (typeof configData !== 'string' && typeof configData !== 'object') {
      console.log('Your configuration data are not valid');
      return false;
    }

    if (typeof configData === 'object') {
      if (!configData.hasOwnProperty('private_token') ||
        !configData.hasOwnProperty('gitlab_api') || !configData.hasOwnProperty('project_id')) {
        console.log('Your configuration data are not valid');
        return false;
      }
    } else if (typeof configData !== 'string') {
      console.log('Your configuration data are not valid');
      return false;
    }

    return true;
  }
};

module.exports = ConfigBuilder;
