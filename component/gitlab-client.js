'use strict';
var nodeGitlab = require('node-gitlab');
var q = require('q');

var GitlabClient = function GitLabClient(gitlabConfig) {
  this.gitlabConfig = gitlabConfig;
  this.gitlabApi = gitlabConfig.gitlab_api;
  this.privateToken = gitlabConfig.private_token;
  this.arrExclusionPtrns = gitlabConfig.branch_filter;
  this.connection = {};
  this.projectBranchesList = [];
};

GitlabClient.prototype = {

  /**
   * build a connection to gitlan using the config data
   */
  buildConnection: function buildConnection() {
    this.connection = nodeGitlab.create({
      api: this.gitlabApi,
      privateToken: this.privateToken
    });
  },

  /**
   * retrieve the branches of the project specified in the config data
   */
  retrieveProjectBranches: function retrieveProjectBranches() {
    var deferred = q.defer();
    this.connection.repository.getBranches({
      id: this.gitlabConfig.project_id
    }, function(err, projectBranches) {
      if (err) {
        console.log('something went wrong while retrieving branches of your project');
        console.log(err.message);
        deferred.reject(new Error(err));
        process.exit(1);
      }

      var projectBranchesList = [];
      var branchCounter;
      var length = projectBranches.length;

      for (branchCounter = 0; branchCounter < length; branchCounter++) {
        if (!this.testExclusionPattern(this.arrExclusionPtrns, projectBranches[branchCounter])) {
          console.log('Retrieving branch: ' + projectBranches[branchCounter].name);
          projectBranchesList.push(projectBranches[branchCounter].name);
        }
      }

      console.log('Retrieved ' + projectBranchesList.length + ' branches for project ' + this.gitlabConfig.project_id);
      this.projectBranchesList = projectBranchesList;
      console.log(projectBranchesList);
      deferred.resolve();
    }.bind(this));

    return deferred.promise;
  },

  testExclusionPattern: function testExclusionPattern(arrExclusionPtrns, projectBranch) {
    console.log(projectBranch.name);
    var ptrnCount;
    var arrPtnLength = arrExclusionPtrns.length;

    for (ptrnCount = 0; ptrnCount < arrPtnLength; ptrnCount++) {
      if (projectBranch.name.match(arrExclusionPtrns[ptrnCount])) {
        return true;
      }
    }
    return false;
  }
};

module.exports = GitlabClient;
