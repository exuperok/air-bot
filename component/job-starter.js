'use strict';
var IMPERATIF = 'You need to provide a config file preceeded by the -c option';

var JobStarter = function JobStarter(commandsArgs) {
  this.arguments = commandsArgs;
  this.projectName = '';
};

JobStarter.prototype = {
  /**
   * start an autojob process
   * @returns string
   */
  startProcess: function startProcess() {
    if (this.arguments.c && (typeof this.arguments.c === 'string')) {
      console.log('Autojob process is starting now.');
      return this.arguments.c
    } else {
      throw new Error('Your project name is not valid');
    }
  }
};

module.exports = JobStarter;
