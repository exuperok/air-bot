'use strict';
var fs = require('fs');
var DATA_FOLDER = './data/';
var PREV_LIST_FILE = 'prev.list.json';

var DiffBuilder = function DiffBuilder(branchList, projectName) {
  this.currBranchList = branchList;
  this.projectName = projectName;
  this.prevBranchListstr = '';
  this.prevBranchList = [];
  this.branchDiff = [];
};

DiffBuilder.prototype = {

  /**
   * build a diff between previous and current branch list
   * @returns {boolean}
   */
  buildBranchDifff: function buildDifff() {
    console.log('trying to build the diff betwen current and previous branches list');

    if (this.prevBranchList.length === 0) {
      console.log('The previous branchlist is empty');
      this.branchDiff = {
        deletion_list: [],
        creation_list: this.currBranchList
      };
      console.log('This projects diff amounts to ' + this.branchDiff.creation_list.length + ' branches created');
    } else {
      var deletionList = this.buildDeletionList(this.currBranchList, this.prevBranchList);
      var creationList = this.buildCreationList(this.currBranchList, this.prevBranchList);

      this.branchDiff = {
        deletion_list: deletionList,
        creation_list: creationList
      };

      if (this.branchDiff.deletion_list.length === 0 && this.branchDiff.creation_list.length === 0) {
        console.log('There is no difference between previous and current branch list of project ' + this.projectName);
        process.exit(0);
      } else {
        console.log('This projects diff amounts to ' + this.branchDiff.deletion_list.length + ' branches deleted');
        console.log('This projects diff amounts to ' + this.branchDiff.creation_list.length + ' branches created');
      }
    }
    if(this.currBranchList.length) {
      this.saveBranchList()
    }
    return true;
  },

  /**
   * load previous branches list from
   */
  loadPrevList: function loadPrevList() {
    try {
      this.prevBranchListStr = fs.readFileSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE, 'utf8');
      console.log('Your previous branch list has been loaded for parsing');
    } catch (e) {
      console.log(e.message);
      console.log('It seems that you do not have a previous list, autojob will atempt to create & load one');
      this.createPrevList();
      this.prevBranchListStr = fs.readFileSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE, 'utf8');
    }
    return true;
  },

  /**
   * Parse the previous list of branches in the gitlab project
   * @returns {boolean}
   */
  parsePrevList: function parsePrevList() {
    if (this.prevBranchListStr !== '' && typeof this.prevBranchListStr !== 'undefined') {
      try {
        console.log('Parsing your previous branches list');
        var tmpProjectBranch = JSON.parse(this.prevBranchListStr);
        if (typeof tmpProjectBranch === 'object') {
          this.prevBranchList = tmpProjectBranch;
          console.log('Your previous list has been parsed');
        }
      } catch (e) {
        console.log(e.message);
        console.log('Something went wrong while trying to parse your previous list');
        process.exit(2);
      }
    } else {
      this.prevBranchList = [];
    }

    return true;
  },

  /**
   * Save the current branch list into prev.list.json file
   * @param systemConfig
   */
  saveBranchList: function saveBranchList() {
    if (this.currBranchList) {
      try {
        var prevList = JSON.stringify(this.currBranchList);
        fs.writeFileSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE, prevList);
        console.log('current branch successfully saved as previous.');
      } catch (e) {
        console.log('Something went wrong while attempting to save the latest branch list to .');
        console.log(e.message);
        process.exit(1);
      }
    } else {
      console.log('Something went wrong while attempting to save the latest branch list to file.');
    }
  },

  /**
   * build the deletion list based on branches not present in the current branch list
   * @param currBranchList
   * @param prevBranchList
   * @returns {*|Array.<T>}
   */
  buildCreationList: function buildCreationList(currBranchList, prevBranchList) {
    var deletionListList = currBranchList.filter(function(cBranch) {
      return !prevBranchList.some(function(pBranch) {
        return cBranch === pBranch;
      });
    });
    return deletionListList;
  },

  /**
   * build the creation list based on the branches not presenty in the previous branch list
   * @param prevBranchList
   * @param currBranchList
   * @returns {*|Array.<T>}
   */
  buildDeletionList: function builDeletionList(currBranchList, prevBranchList) {
    var creationList = prevBranchList.filter(function(pBranch) {
      return !currBranchList.some(function(cBranch) {
        return pBranch === cBranch;
      });
    });
    return creationList;
  },

  createPrevList: function createPrevList() {
    if (!fs.existsSync(DATA_FOLDER + this.projectName)) {
      try {
        console.log('Creating the project data folder and previous list file');
        fs.mkdirSync(DATA_FOLDER + this.projectName);
        fs.writeFileSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE, '');
      } catch (e) {
        console.log(e.message);
        console.log('Something went wrong while attempting to create your data folder and file');
      }
    } else if (!fs.existsSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE)) {
      try {
        console.log('Creating the previous list file');
        fs.writeFileSync(DATA_FOLDER + this.projectName + '/' +PREV_LIST_FILE, '');
      } catch (e) {
        console.log(e.message);
        console.log('Something went wrong while attempting to create your data folder and file');
      }
    }
  }
};

module.exports = DiffBuilder;
