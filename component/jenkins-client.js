'use strict';

var JenkinsClient = function JenkinsClient(jenkinsConfig, currBranchList, projectName, systemConfig) {
  this.jenkinsConfig = jenkinsConfig;
  this.systemConfig = systemConfig;
  this.currBranchList = currBranchList;
  this.projectName = projectName;
  this.connection = {};
};

JenkinsClient.prototype = {

  buildConnection: function buildConnection() {
    this.connection = require('jenkins')(this.jenkinsConfig.protocol +
    this.jenkinsConfig.username + ':' + this.jenkinsConfig.password + '@' + this.jenkinsConfig.jenkins_url);
  },

  /**
   * create a job from the creation_list
   * @param jobToCreate
   * @param branchDiff
   * @param diffCount
   */
  createProjectJob: function createProjectJob(creation_list, diffCount) {
    var template = this.jenkinsConfig.template_job;

    this.connection.job.exists(template, function(err, exists) {
      if (err) {
        throw err;
      }

      if (exists) {
        if (diffCount < creation_list.length) {
          var jobToCreate = creation_list[diffCount].replace(/\//g, '-');
          var branchSpec = creation_list[diffCount];

          this.connection.job.exists(jobToCreate, function(err, exists) {
            if (err) {
              throw err;
            } else if (!exists) {
              this.connection.job.config(template, function(err, configXml) {
                if (err) {
                  throw err;
                }

                var UpdatedConfigXml = this.updateJobConfig(configXml, branchSpec);
                this.connection.job.create({name: jobToCreate, xml: UpdatedConfigXml}, function(err) {
                  if (err) {
                    throw err;
                  }

                  console.log('Created jenkins job for gitlab project branch ' + jobToCreate);
                }.bind(this));
                this.createProjectJob(creation_list, diffCount + 1);
              }.bind(this));
            } else {
              this.createProjectJob(creation_list, diffCount + 1);
            }
          }.bind(this));
        } else {
          console.log(creation_list.length + ' jobs created. Creation process is ending.');
        }
      } else {
        console.log('The master job provided in your config does not exist.');
        process.exit(1);
      }
    }.bind(this));
  },

  /**
   * Delete a job from deletion_list
   * @param jobToCreate
   * @param diffCount
   * @param branchDiff
   * @param branchSpec
   */
  deleteProjectJob: function deleteProjectJob(deletion_list, diffCount) {
    var masterJob = this.jenkinsConfig.template_job;

    this.connection.job.exists(masterJob, function(err, exists) {
      if (err) {
        console.log('info');
        throw err;
      }

      if (exists) {
        if (diffCount < deletion_list.length) {
          var jobToDelete = deletion_list[diffCount].replace(/\//g, '-');

          this.connection.job.exists(jobToDelete, function(err, exists) {
            if (err) {
              throw err;
            } else if (exists) {
              this.connection.job.config(jobToDelete, function(err, configXml) {
                if (err) {
                  throw err;
                }

                if (this.checkIsAutojob(configXml) !== undefined) {
                  this.connection.job.destroy(jobToDelete, function(err) {
                    if (err) {
                      throw err;
                    }
                    console.log('Job ' + jobToDelete + ' has been deleted from the jenkins project.');
                  }.bind(this));
                }
                this.deleteProjectJob(deletion_list, diffCount + 1);
              }.bind(this));
            } else {
                this.deleteProjectJob(deletion_list, diffCount + 1);
            }
          }.bind(this));
        } else {
          console.log(deletion_list.length + ' jobs have been deleted. Deletion process is ending');
        }
      } else {
        console.log('The master job provided in your config does not exist.');
        process.exit(1);
      }
    }.bind(this));
  },

  /**
   *
   * @param configXml
   * @param branchSpec
   * @returns {*}
   */
  updateJobConfig: function updateJobConfig(configXml, branchSpec) {
    //update the branch name of the job to the apropriate one
    var DOMParser = require('xmldom').DOMParser;
    var doc = new DOMParser().parseFromString(configXml);

    if (doc) {
      this.changeBranchName(doc, branchSpec);
      this.enableCreatedJob(doc)
      this.addAutojobFlag(doc);
      var serializedXML = this.serializeConfig(doc, branchSpec);
      return serializedXML;
    }
  },

  /**
   * check if the job is an autojob with the presence of autojob tag in its config
   * @param configXml
   * @returns {*}
   */
  checkIsAutojob: function checkIsAutojob(configXml) {
    var DOMParser = require('xmldom').DOMParser;
    var doc = new DOMParser().parseFromString(configXml);
    var isAutojob = doc.getElementsByTagName('isAutojob')[0];

    return isAutojob;
  },

  enableCreatedJob: function enableCreatedJob(doc) {
    var disabledData = doc.getElementsByTagName('disabled')[0].childNodes[0];

    if(doc.getElementsByTagName('disabled')[0].childNodes[0].data){
      doc.getElementsByTagName('disabled')[0].childNodes[0].data = 'false';
    }
  },

  /**
   * Flag every job that it creates to seperate them from manualy created jobs
   * @param doc
   */
  addAutojobFlag: function addAutojobFlag(doc) {
    var isAutojob = doc.createElement('isAutojob');
    var value = doc.createTextNode(1);
    isAutojob.appendChild(value);
    var project = doc.getElementsByTagName('project')[0];
    project.appendChild(isAutojob);
    doc.appendChild(project);

    if (doc.getElementsByTagName('isAutoJob')[0]) {
      console.log('Job has been flagged as Autojob created');
    }
  },

  /**
   *  Change the branch name of the target of the job being created from master to branchSpec
   * @param doc
   * @param branchSpec
   */
  changeBranchName: function changeBranchName(doc, branchSpec) {
    var branchNameElement = doc.getElementsByTagName('hudson.plugins.git.BranchSpec');
    var branchNameNode = branchNameElement.item(0).childNodes.item(1).firstChild;
    branchNameNode.data = branchSpec;

    if (doc.getElementsByTagName(branchNameElement.item(0).childNodes.item(1).firstChild === branchSpec)) {
      console.log('Job has been flagged as Autojob created');
    }
  },

  /**
   * Serialized the updated configuration into an xml string
   * @param doc
   * @param branchSpec
   */
  serializeConfig : function serializeConfig(doc, branchSpec) {
    var XmlSerializer = require('xmldom').XMLSerializer;
    var serializedXML = new XmlSerializer().serializeToString(doc);
    if (serializedXML) {
      console.log('updated configuration of the job with new target branch' + branchSpec);
      return serializedXML;
    }
  }
};

module.exports = JenkinsClient;
